// test program for LCM
     INP
     OUT      // initialize output 

LOOP BRZ QUIT // if the accumulator is zero, jump to the label QUIT
     SUB ONE  // subtract value at ONE from the accumulator
     OUT      // print value in accumulator
     BRA LOOP // jump to the memory address labeled LOOP

QUIT HLT      // label this memory address as QUIT
ONE  DAT 1    // store the value 1 in this memory address, and label it ONE
