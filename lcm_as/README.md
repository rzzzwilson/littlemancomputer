# lcm_as

The assembler for the Little Man Computer simulation.

## Usage

Usage:

    lcm_as  [-h]  <input_file> [<output_file>]

where the <input_file>   is text as described in the Wikipedia article,
and       <output_file>  is a text file of numbers.

## Input file

The format of the input file is a simple text file.  The "assembly language"
is a explained in [the Wikipedia article about LCM](https://en.wikipedia.org/wiki/Little_man_computer#Examples).

## Output file

The output file is a text file consisting of numbers that are loaded sequentially
into the LCM memory starting at address 0.  Memory that isn't loaded from a
file is assumed to contain the "000" opcoce (HLT/COB).
