# lcm_cli

A command line interpreter for the Little Man Computer.

Usage:

    lcm_cli <LCM file>

where the <LCM_file> is a *.lcm file produced by the LCM assembler (lcm_as).
