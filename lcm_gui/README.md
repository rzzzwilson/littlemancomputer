# lcm_gui

A GUI-based interpreter for the Little Man Computer.

Usage:

    lcm_gui

## Overall Design

We want a GUI to allow the editing/assembling/running of LCM
assembler source programs.

* editor that allows loading/saving of ASM program code
* display of AC, PC and mailboxes
* execution allows single-step, slow or fast execution
* display highlights assembler line being executed
* mailbox display highlights read/write operations
* breakpoints (on PC, AC, mailbox)

Use either tkinter or PyQt.

## Editor Design

* show line numbers
* allow highlighting from outside editor (execution highlighting)
* load/save from/to file
* undo (semi-unlimited?)
* set/clear breakpoints
* show assembled info (address, binary opcode) after assembly
* any edit change removes assembler info (but see below)
* "always on" assembler, ie, after edit reassemble? (window for assembler errors)

Work on the editor/assembler **first**.  Leave out the full GUI stuff like
set/clear breakpoints, attachment to the mailbox display, etc, just editor plus
the assembler.

### Initial editor parts

* main display is a multi-line edit pane
* line number at left
* three columns: address, binary opcode, actual code
* only "actual code" column is editable
* tab key spaces to next column (columns hard-coded initialy)
* multi line select, then cut/copy/delete?
* empty display has one line (line 1)
