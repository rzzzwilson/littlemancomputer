# LittleManComputer

A stab at an implementation in python of the Little Man Computer instructional computer model.  It is hoped to have both a command line as well as a GUI implementation.

The [instructional model is explained here](https://en.wikipedia.org/wiki/Little_man_computer).

## Subprojects

### lcm_as

The assembler for the Little Man Computer.  Produces *.lcm files.

### lcm_cli

Command line interpreter for *.lcm files.

### lcm_gui

GUI version of *lcm_cli*.  Using PyQt maybe, or tkinter.
